import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import store from "./store/store";
import './assets/base.css';
import i18n from './i18n/i18n';

new Vue({
    el: '#app',
    router,
    store,
    i18n,
    render: h => h(App)
})